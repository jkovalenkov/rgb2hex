#include "Color.h"


CColor::CColor( void ) {
	m_nR = 0;
	m_nG = 0;
	m_nB = 0;
	m_fDebug = false;
}


CColor::~CColor( void ) {
}


bool CColor::ParseFromArguments( int argc, const char* argv[] ) {
	bool fParsed = false;
	if (m_fDebug) fprintf( stdout, "DEBUG: argc: %d\n", argc );
	if (4 == argc) {
		if (m_fDebug) fprintf( stdout, "DEBUG: Parsing arguments\n" );
		m_nR = GetArgumentValue( argv[1] );
		m_nG = GetArgumentValue( argv[2] );
		m_nB = GetArgumentValue( argv[3] );
		if (m_fDebug) fprintf( stdout, "DEBUG: Parsed values\n\tR = %d\n\tG = %d\n\tB = %d\n", m_nR, m_nG, m_nB );
		if (m_fDebug) fprintf( stdout, "DEBUG: Arguments parsed\n" );
		fParsed = true;
	}
	return fParsed;
}

void CColor::PrintHexValue( void ) {
	fprintf( stdout, "Hex value: #%.2x%.2x%.2x\n", m_nR, m_nG, m_nB );
}

unsigned char CColor::GetArgumentValue( const char* pszArg ) {
	int nValue = 0;
	unsigned char nResult = 0;
	nValue = atoi( pszArg );
	if ((nValue >= 0) && (nValue <= UCHAR_MAX))
		nResult = (unsigned char)nValue;		
	return nResult;
}
