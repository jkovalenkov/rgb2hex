#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

class CColor
{
	//member variables
public:
	unsigned char m_nR;
	unsigned char m_nG;
	unsigned char m_nB;
	bool m_fDebug;

	//methods
public:
	CColor( void );
	~CColor( void );
	bool ParseFromArguments ( int argc, const char* argv[] );
	void PrintHexValue( void );

private:
	unsigned char GetArgumentValue( const char* pszArg );
};

