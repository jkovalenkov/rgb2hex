#include <stdio.h>
#include "Color.h"

void main( int argc, const char* argv[] ) {
	CColor* color = 0;
	if (4 == argc) {
		color = new CColor();
		color->m_fDebug = false;
		if (color->ParseFromArguments( argc, argv ))
			color->PrintHexValue();
	}
	else {
		fprintf( stdout, "Converts RGB value to html hex color value\n\n" );
		fprintf( stdout, "usage: rgb2hex <R value> <G value> <B value>\n\n" );
		fprintf( stdout, "author: Vladimir Kocjancic\n" );
		fprintf( stdout, "url: http://www.lotushints.com\n" );
	}
	if (0 != color)
		delete color;
}